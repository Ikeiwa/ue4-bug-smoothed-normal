#include "BPPhysicUtils.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

 FVector UBPPhysicUtils::GetSmoothNormalFromTrace(const FHitResult & HitResult)
{
	 FVector Normal = HitResult.Normal.GetSafeNormal();

	if (!HitResult.bBlockingHit)
		return HitResult.Normal;

	UStaticMeshComponent* StaticMeshComp = Cast<UStaticMeshComponent>(HitResult.GetComponent());

	if (!StaticMeshComp) 
		return HitResult.Normal;

	// Retrieve static mesh.
	UStaticMesh* StaticMesh = StaticMeshComp->GetStaticMesh();

	// Return if the static mesh isn't set (shouldn't happen)
	if (!StaticMesh)
		return HitResult.Normal;

	// In cooked builds we need to have this flag set or we'll crash when trying to access mesh data
	if (!StaticMesh->bAllowCPUAccess)
		return HitResult.Normal;

	// Return if RenderData is invalid
	if (!StaticMesh->RenderData)
		return HitResult.Normal;

	// No valid mesh data on lod 0 (shouldn't happen)
	if (!StaticMesh->RenderData->LODResources.IsValidIndex(0))
		return HitResult.Normal;

	int FaceIndex = HitResult.FaceIndex;
	FTransform ComponentTransform = StaticMeshComp->GetComponentTransform();
	FStaticMeshVertexBuffers* VertexBuffers = &StaticMesh->RenderData->LODResources[0].VertexBuffers;
	FStaticMeshVertexBuffer* VertexBuffer = &VertexBuffers->StaticMeshVertexBuffer;
	FPositionVertexBuffer* PositionVertexBuffer = &VertexBuffers->PositionVertexBuffer;
	FIndexArrayView IndexBuffer = StaticMesh->RenderData->LODResources[0].IndexBuffer.GetArrayView();

	FVector VertexPositions[3];
	FVector VertexNormals[3];

	for (int i = 0; i < 3; i++) {
		// Get vertex index
		uint32 index = IndexBuffer[FaceIndex * 3 + i];
		// Get vertex position and normal
		VertexPositions[i] = PositionVertexBuffer->VertexPosition(index);
		VertexNormals[i] = VertexBuffer->VertexTangentZ(index);

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("vertex %i : %s | %s"), i, *VertexPositions[i].ToString(), *VertexNormals[i].ToString()));

		// Transform position and normal into world space
		VertexPositions[i] = ComponentTransform.TransformPosition(VertexPositions[i]);
		VertexNormals[i] = ComponentTransform.TransformVector(VertexNormals[i]);
	}

	// Determine the barycentric coordinates
	/*FVector U = VertexPositions[1] - VertexPositions[0];
	FVector V = VertexPositions[2] - VertexPositions[0];
	FVector W = HitResult.ImpactPoint - VertexPositions[0];

	FVector vCrossW = FVector::CrossProduct(V, W);
	FVector vCrossU = FVector::CrossProduct(V, U);

	if (FVector::DotProduct(vCrossW, vCrossU) < 0.0f) {
		return HitResult.Normal;
	}

	FVector uCrossW = FVector::CrossProduct(U, W);
	FVector uCrossV = FVector::CrossProduct(U, V);

	if (FVector::DotProduct(uCrossW, uCrossV) < 0.0f) {
		return HitResult.Normal;
	}

	float Denom = uCrossV.Size();
	float b1 = vCrossW.Size() / Denom;
	float b2 = uCrossW.Size() / Denom;
	float b0 = 1.0f - b1 - b2;*/

	FVector baryCoords = FMath::GetBaryCentric2D(HitResult.ImpactPoint, VertexPositions[0], VertexPositions[1], VertexPositions[2]);

	// Determine the hit normal
	Normal = baryCoords.X * VertexNormals[0] + baryCoords.Y * VertexNormals[1] + baryCoords.Z * VertexNormals[2];


	// Just to be safe here
	if (!Normal.Normalize())
		return HitResult.Normal;

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("%f, %f, %f"), Normal.X, Normal.Y, Normal.Z));

	return Normal;
}