#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EngineMinimal.h"
#include "BPPhysicUtils.generated.h"

UCLASS()
class UBPPhysicUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Physics")
		 static FVector GetSmoothNormalFromTrace(const FHitResult& HitResult);
};
